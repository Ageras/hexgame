
/*
Dev: Jeroen Hoekstra
Program: Hex Game
Assignment for algorithmics and concurrency
*/
#include "HexBoard.h"
#include "Dijkstra.h"
#include "InputListener.h"
#include "TurnManager.h"
#include "Minimax.h"
#include "MonteCarlo.h"

#include <iostream>
#include <vector>
#include <string>
#include <thread>

int main()
{
	MonteCarlo monteCarlo = MonteCarlo();

	//Initialize all the needed variables
	HexBoard hexBoard = HexBoard();
	Dijkstra dijkstra = Dijkstra();
	InputListener inputListener = InputListener();
	Minimax minimax = Minimax();
	std::thread worker;

	std::string player;
	int currentTurn = 1;
	char user = 'R';
	bool winner = false;

	//Initialize the board
	std::cout << "WELCOME TO HEXGAME++" << std::endl;
	hexBoard.generateBoard();
	hexBoard.drawBoard();
	std::cout << "Start with Player or AI?" << std::endl;
	std::cin >> player;

	//Game loop
	while (true)
	{
		std::cout << "Turn " << currentTurn << std::endl;
		std::cout << "Current turn is for: " << player << std::endl;
		user = hexBoard.getUser(currentTurn); // get R or B
		//Determine wether Player or AI gets next turn
		if (player == "Player")
		{
			TurnManager::playerTurn(hexBoard, inputListener, user, player, currentTurn);
		}
		else
		{
			TurnManager::aiTurn(hexBoard, inputListener, user, currentTurn, player);
		}
		//Check if red is a winner, run in a different thread so blue can be checked simultaniously
		worker = std::thread(&TurnManager::checkRed, dijkstra, hexBoard);
		//Check if blue is a winner
		TurnManager::checkBlue(dijkstra, hexBoard);
		worker.join();
		hexBoard.displayLastMove();
		//Swap player and AI
		player = TurnManager::nextPlayer(player);
	}
	return 0;
}