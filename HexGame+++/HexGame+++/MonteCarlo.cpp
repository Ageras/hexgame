#include "MonteCarlo.h"
#include "Dijkstra.h"

#include <iostream>

MonteCarlo::MonteCarlo()
{
	gen = std::mt19937(rd());
}

MonteCarlo::~MonteCarlo()
{
}

int MonteCarlo::calcScore(std::vector<std::vector<char>> gameState, int boardSize, char player)
{
	std::atomic<int> wins = 0;
	//not atomic, will be handled with mutexes
	int losses = 0;
	int iterations = 100;
	int numberOfThreads = 5;
	int start = 0;
	int end = iterations / numberOfThreads;

	//create threads
	workers.push_back(std::thread(&MonteCarlo::threadedScore, this, gameState, boardSize, player, std::ref(wins), std::ref(losses), start, end));
	workers.push_back(std::thread(&MonteCarlo::threadedScore, this, gameState, boardSize, player, std::ref(wins), std::ref(losses), end, end * 2));
	workers.push_back(std::thread(&MonteCarlo::threadedScore, this, gameState, boardSize, player, std::ref(wins), std::ref(losses), end * 2, end * 3));
	workers.push_back(std::thread(&MonteCarlo::threadedScore, this, gameState, boardSize, player, std::ref(wins), std::ref(losses), end * 3, end * 4));
	workers.push_back(std::thread(&MonteCarlo::threadedScore, this, gameState, boardSize, player, std::ref(wins), std::ref(losses), end * 4, end * 5));

	//wait for threads to join
	for (auto &th : workers)
	{
		th.join();
	}

	//return the result
	return wins - losses;
}

void MonteCarlo::threadedScore(std::vector<std::vector<char>> gameState, int boardSize, char player, std::atomic<int>& wins, int& losses, int start, int end)
{
	for (int k = start; k < end; k++)
	{
		if (monteCarlo(gameState, boardSize, player))
		{
			//if montecarlo returns true (you win) increment wins
			wins++;
		}
		else
		{
			//lock the (shared) losses variable
			mutex.lock();
			//if montecarlo returns false (you lose) increment losses
			losses++;
			//unlock the (shared) losses variable
			mutex.unlock();
		}
	}
}

bool MonteCarlo::monteCarlo(std::vector<std::vector<char>> gameState, int boardSize, char player)
{
	Dijkstra dijkstra = Dijkstra();
	//random numbers go from 0 to boardSize-1
	std::uniform_int_distribution<> dist(0, boardSize - 1);
	//create a copy of the gameState
	std::vector<std::vector<char>> copyState = gameState;
	//create a copy of the Player so you can swap the player but still check winner for original player
	char copyPlayer = player;

	for (int i = 0; i < boardSize * boardSize; i++)
	{
		//If the random move is not already taken
		if ((copyState[dist(gen)])[dist(gen)] == '-')
		{
			//Make the random move 'R' or 'B' and check if you win or lose
			(copyState[dist(gen)])[dist(gen)] = copyPlayer;

			if (player == 'R' && dijkstra.redWin(copyState, boardSize))
			{
				//return true if player = R and he wins
				return true;
			}
			else if (dijkstra.blueWin(copyState, boardSize))
			{
				//return false if player = R and he loses
				return false;
			}
			if (player == 'B' && dijkstra.blueWin(copyState, boardSize))
			{
				//return true if player = B and he wins
				return true;
			}
			else if (dijkstra.redWin(copyState, boardSize))
			{
				//return false if player = B and he loses
				return false;
			}
			//Swap B/R
			copyPlayer = nextPlayer(copyPlayer);
		}
	}
}

//swap around the players
char MonteCarlo::nextPlayer(char player)
{
	if (player == 'R')
	{
		return 'B';
	}
	return 'R';
}
