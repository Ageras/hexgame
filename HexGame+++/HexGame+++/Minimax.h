/*
Class that determines the best possible move
*/
#pragma once

#include "Dijkstra.h"
#include <vector>

class Minimax
{
public:
	Minimax();
	~Minimax();

	struct Move
	{
		int row;
		int col;
	};

	int score(std::vector<std::vector<char>>& gameState, int row, int col, int& depth, char& color, int boardSize);
	Move minimax(std::vector<std::vector<char>> gameState, int depth, char& color, int boardSize, std::string player);
	std::vector<std::vector<bool>> getAvailableMoves(std::vector<std::vector<char>> gameState, int boardSize);
	std::vector<std::vector<char>> getNewState(int& row, int& col, std::vector<std::vector<char>> gameState, char& color);
	std::vector<std::vector<char>> getNewOppositeState(int& row, int& col, std::vector<std::vector<char>> gameState, char& color);
	bool pieRule();

private:
	Dijkstra dijkstra = Dijkstra();
	Move move;
};