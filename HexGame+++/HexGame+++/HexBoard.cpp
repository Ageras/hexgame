#include "HexBoard.h"

#include <iostream>

HexBoard::HexBoard()
{
}


HexBoard::~HexBoard()
{
}

void HexBoard::generateBoard()
{
	board = std::vector<std::vector<char>>(boardSize, std::vector<char>(boardSize));
	std::cout << "" << std::endl;
	for (int x = 0; x < boardSize; x++)
	{
		for (int y = 0; y < boardSize; y++)
		{
			(board[x])[y] = '-';
		}
	}
}

//Method that draws board based on our vector
void HexBoard::drawBoard()
{
	int tempRow;
	std::cout << "             RED" << std::endl;
	std::cout << "    A B C D E F G H I J K" << std::endl;
	for (int row = 0; row < boardSize; row++)
	{
		tempRow = row;
		if (tempRow >= 9)
		{
			tempRow--;
		}
		for (int i = 0; i < tempRow; i++)
		{
			std::cout << " ";
		}
		std::cout << 1 + row << " | ";
		for (int col = 0; col < boardSize; col++)
		{
			std::cout << board[row][col] << " ";
		}
		std::cout << "| " << row + 1;
		if (row == 5)
		{
			std::cout << " BLUE" << std::endl;
		}
		else
		{
			std::cout << std::endl;
		}
	}
	std::cout << "              A B C D E F G H I J K" << std::endl;
}

//Add move to the board
void HexBoard::changeBoard(int row, char col, char newVal)
{
	(board[col])[row] = newVal;
}

//Store a new move
void HexBoard::addMove(std::string move)
{
	previousMoves.push_back(move);
}

void HexBoard::displayLastMove()
{
	if (!previousMoves.empty())
	{
		std::cout << "Last move was: " << previousMoves[previousMoves.size() - 1] << std::endl;
	}
}

//Undo the last move
void HexBoard::undo(InputListener iL, std::string player)
{
	if (!previousMoves.empty())
	{
		std::string input = previousMoves[previousMoves.size() - 1];
		char inputCol = input[0];
		std::string inputRow = input.substr(1, 2);
		changeBoard(iL.getProgramOutput(inputCol), atoi(inputRow.c_str()) - 1, '-');
		previousMoves.erase(previousMoves.end() - 1);
	}
}

void HexBoard::gameOver()
{
	std::cout << "Game is over, close the program!";
	while (true){};
}

//Return the user for the current turn
char HexBoard::getUser(int turn)
{
	if (turn % 2 == 0)
	{
		return 'B';
	}
	return 'R';
}

int HexBoard::getBoardSize()
{
	return boardSize;
}

std::vector<std::vector<char>> HexBoard::getBoard()
{
	return board;
}