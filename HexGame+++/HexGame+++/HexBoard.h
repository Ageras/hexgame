/*
Class where the hexboard is defined
*/
#pragma once

#include "InputListener.h"
#include <vector>
#include <string>

class HexBoard
{
public:
	int getBoardSize();
	int getLastMove();
	void generateBoard();
	void drawBoard();
	void changeBoard(int row, char col, char newVal);
	void displayLastMove();
	void addMove(std::string move);
	void undo(InputListener iL, std::string player);
	std::vector<std::vector<char>> getBoard();
	char getUser(int turn);
	void gameOver();
	HexBoard();
	~HexBoard();

private:
	const int boardSize = 11;
	std::vector<std::vector<char>> board;
	std::vector<std::string> previousMoves;
};

