#pragma once

#include <vector>
#include <random>
#include <thread>
#include <mutex>
#include <atomic>

class MonteCarlo
{
public:
	MonteCarlo();
	~MonteCarlo();

	int calcScore(std::vector<std::vector<char>> gameState, int boardSize, char player);
	void threadedScore(std::vector<std::vector<char>> gameState, int boardSize, char player, std::atomic<int>& wins, int& losses, int start, int end);
	bool monteCarlo(std::vector<std::vector<char>> gameState, int boardSize, char player);
	char nextPlayer(char player);

private:
	std::random_device rd;
	//seed the twister engine
	std::mt19937 gen;
	std::vector<std::thread> workers;
	std::mutex mutex;
};

