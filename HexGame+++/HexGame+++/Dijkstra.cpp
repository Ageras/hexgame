#include "Dijkstra.h"

#include <iostream>

Dijkstra::Dijkstra()
{
}

Dijkstra::~Dijkstra()
{
}

bool Dijkstra::redWin(std::vector<std::vector<char>> boardState, int boardSize)
{
	int i = 0;
	//Check if there's a starting point for red
	for (int j = 0; j < boardState.size(); j++)
	{
		if ((boardState[i])[j] == 'R') //if starting point has been found check if we can win
		{
			checkedCoordinates.clear();
			Coordinate start;
			start.row = i;
			start.col = j;
			if (checkIfWon('R', boardState, start, boardSize))
			{
				return true;
			}
		}
	}
	return false;
}

bool Dijkstra::blueWin(std::vector<std::vector<char>> boardState, int boardSize)
{
	int i = 0;
	//Check if there's a starting point for blue
	for (int j = 0; j < boardState.size(); j++)
	{
		if ((boardState[j])[i] == 'B') //if starting point has been found check if we can win
		{
			checkedCoordinates.clear();
			Coordinate start;
			start.row = j;
			start.col = i;
			if (checkIfWon('B', boardState, start, boardSize))
			{
				return true;
			}
		}
	}
	return false;
}

//Method to determine if blue or red can win from starting position
bool Dijkstra::checkIfWon(char color, std::vector<std::vector<char>> boardState, Dijkstra::Coordinate current, int boardSize)
{
	Coordinate coordinate = current;
	checkedCoordinates.push_back(coordinate); //Vector of all checked coordinates
	if (color == 'R' && current.row == boardSize - 1) //if checking for 'R' and your in last row, see if you can win
	{
		if (current.col != 0)
		{
			if (getColor(getNeighbour(current, left), boardState) == color) //Check left neighbour
			{
				return true;
			}
			if (current.col != boardSize - 1)
			{
				if (getColor(getNeighbour(current, right), boardState) == color)//Check right neighbour
				{
					return true;
				}
			}
			if (getColor(getNeighbour(current, currentCoordinate), boardState) == color)//Check the current coordinate
			{
				return true;
			}
			return false;
		}
		else if (current.col == 0)
		{
			if (getColor(getNeighbour(current, right), boardState) == color)//Check right neighbour
			{
				return true;
			}
			if (getColor(getNeighbour(current, currentCoordinate), boardState) == color)//Check the current coordinate
			{
				return true;
			}
			return false;
		}
	}
	else if (color == 'B' && current.col == boardSize - 1) //if checking for 'B' and your in last col, see if you can win
	{
		if (current.col != 0)
		{
			if (getColor(getNeighbour(current, left), boardState) == color) //Check left neighbour
			{
				return true;
			}
			if (current.col != boardSize - 1)
			{
				if (getColor(getNeighbour(current, right), boardState) == color)//Check right neighbour
				{
					return true;
				}
			}
			if (getColor(getNeighbour(current, currentCoordinate), boardState) == color)//Check the current coordinate
			{
				return true;
			}
			return false;
		}
		else if (current.col == 0)
		{
			if (current.col != boardSize - 1)
			{
				if (getColor(getNeighbour(current, right), boardState) == color)//Check right neighbour
				{
					return true;
				}
			}
			if (getColor(getNeighbour(current, currentCoordinate), boardState) == color)//Check the current coordinate
			{
				return true;
			}
			return false;
		}
	}
	else //if last row has not been reached keep looking for connections
	{
		if (current.row != 0) //Row can't be 0 if checking topleft and topright
		{
			if (getColor(getNeighbour(current, topLeft), boardState) == color && !done(getNeighbour(current, topLeft))) //Check top-left neighbour, if it has not been checked before
			{
				if (checkIfWon(color, boardState, getNeighbour(current, topLeft), boardSize))
				{
					return true;
				}
			}
			if (current.col != boardSize - 1) //Can't be in the last colum if checking topright
			{
				if (getColor(getNeighbour(current, topRight), boardState) == color && !done(getNeighbour(current, topRight)))//Check top-right neighbour, if it has not been checked before
				{
					if (checkIfWon(color, boardState, getNeighbour(current, topRight), boardSize))
					{
						return true;
					}
				}
			}
		}
		if (current.col != 0) //Col can't be 0 if checking botleft and left
		{
			if (current.row != boardSize - 1) //Can't be in the last row if checking botleft
			{
				if (getColor(getNeighbour(current, botLeft), boardState) == color && !done(getNeighbour(current, botLeft))) //Check bot-left neighbour, if it has not been checked before
				{
					if (checkIfWon(color, boardState, getNeighbour(current, botLeft), boardSize))
					{
						return true;
					}
				}
			}
			if (getColor(getNeighbour(current, left), boardState) == color && !done(getNeighbour(current, left))) //Check left neighbour, if it has not been checked before
			{
				if (checkIfWon(color, boardState, getNeighbour(current, left), boardSize))
				{
					return true;
				}
			}
		}

		if (current.row != boardSize - 1) //Can't be in the last row if checking botright
		{
			if (getColor(getNeighbour(current, botRight), boardState) == color && !done(getNeighbour(current, botRight))) //Check bot-right neighbour, if it has not been checked before
			{
				if (checkIfWon(color, boardState, getNeighbour(current, botRight), boardSize))
				{
					return true;
				}
			}
		}

		if (current.col != boardSize - 1)//Can't be in the last col if checking right
		{

			if (getColor(getNeighbour(current, right), boardState) == color && !done(getNeighbour(current, right))) //Check right neighbour, if it has not been checked before
			{
				if (checkIfWon(color, boardState, getNeighbour(current, right), boardSize))
				{
					return true;
				}
			}
		}
		return false;
	}
}

//Method to check if a coordinate has already been checked
bool Dijkstra::done(Dijkstra::Coordinate coordinate)
{
	for (int i = 0; i < checkedCoordinates.size(); i++)
	{
		if (checkedCoordinates[i].row == coordinate.row && checkedCoordinates[i].col == coordinate.col)
		{
			return true;
		}
	}
	return false;
}

//Method to return a current coordinate's neighbours
Dijkstra::Coordinate Dijkstra::getNeighbour(Coordinate current, Dijkstra::neighbours direction)
{
	switch (direction)
	{
	case topLeft:
		return(Coordinate{ current.row - 1, current.col });
	case topRight:
		return(Coordinate{ current.row - 1, current.col + 1 });
	case left:
		return(Coordinate{ current.row, current.col - 1 });
	case right:
		return(Coordinate{ current.row, current.col + 1 });
	case botLeft:
		return(Coordinate{ current.row + 1, current.col - 1 });
	case botRight:
		return(Coordinate{ current.row + 1, current.col });
	case currentCoordinate:
		return(Coordinate{ current.row, current.col });
	}
}

char Dijkstra::getColor(Dijkstra::Coordinate coordinate, const std::vector<std::vector<char>>& boardState)
{
	return (boardState[coordinate.row])[coordinate.col];
}