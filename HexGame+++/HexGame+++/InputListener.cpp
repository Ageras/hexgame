#include "InputListener.h"

#include <string>

InputListener::InputListener()
{
	programOutput = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	userInput = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K' };
}


InputListener::~InputListener()
{
}

//Make the users input readable for our vector
int InputListener::getProgramOutput(char input)
{
	input = toupper(input);
	for (int i = 0; i < programOutput.size(); i++)
	{
		if (input == userInput[i])
		{
			return programOutput[i];
		}
	}
}

//Convert a row and col to a move
std::string InputListener::toMove(int row, int col)
{
	std::string output;
	for (int i = 0; i < userInput.size(); i++)
	{
		if (row == programOutput[i])
		{
			output += userInput[i];
		}
	}
	output += std::to_string(col + 1);
	return output;
}