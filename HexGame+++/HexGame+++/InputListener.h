/*
Class to handle/convert all the input given to it
*/
#pragma once

#include <vector>

class InputListener
{
public:
	InputListener();
	~InputListener();
	int getProgramOutput(char input);
	std::string toMove(int row, int col);

private:
	std::vector<char> userInput;
	std::vector<int> programOutput;
};

