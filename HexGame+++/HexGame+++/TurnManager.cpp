#include "TurnManager.h"

TurnManager::TurnManager()
{
}


TurnManager::~TurnManager()
{
}

void TurnManager::playerTurn(HexBoard& hexBoard, InputListener& inputListener, char& user, std::string& player, int& turn)
{
	std::string inputRow;
	char inputCol;
	std::string input;
	bool pie;
	char undo;
	Minimax minimax = Minimax();

	std::cout << "Enter your turn (ex, A1)" << std::endl;
	//Put players turn into input variable
	std::cin >> input;
	hexBoard.addMove(input);
	inputCol = input[0];
	inputRow = input.substr(1, 2);
	//Add players turn to the playing board
	hexBoard.changeBoard(inputListener.getProgramOutput(inputCol), atoi(inputRow.c_str()) - 1, user);
	system("cls");
	hexBoard.drawBoard();

	//if turn is 1, see if pie rule should be applied
	if (turn == 1)
	{
		std::cout << "Pie? y/n" << std::endl;
		pie = minimax.pieRule();
		if (pie)
		{
			player = "AI";
		}
	}

	std::cout << "undo this turn?" << std::endl;
	std::cin >> undo;

	//Determine wether to undo or not
	if (undo == 'y')
	{
		hexBoard.undo(inputListener, player);
		player = "AI";
		turn--;
	}

	system("cls");
	hexBoard.drawBoard();
	turn++;
}

void TurnManager::aiTurn(HexBoard& hexBoard, InputListener& inputlistener, char& user, int& turn, std::string& player)
{
	Minimax minimax = Minimax();
	char pie;
	//Determine AI's next move
	Minimax::Move aImove = minimax.minimax(hexBoard.getBoard(), 0, user, hexBoard.getBoardSize(), player);
	//Add move to the vector of all moves
	hexBoard.addMove(inputlistener.toMove(aImove.col, aImove.row));
	//add AI's turn to the board
	hexBoard.changeBoard(aImove.col, aImove.row, user);
	system("cls");
	hexBoard.drawBoard();

	//if turn is 1, see if pie rule should be applied
	if (turn == 1)
	{
		std::cout << "Pie? y/n" << std::endl;
		std::cin >> pie;
		if (pie == 'y')
		{
			player = "Player";
		}
	}
	system("cls");
	hexBoard.drawBoard();
	turn++;
}

//Check if red wins
bool TurnManager::checkRed(Dijkstra dijkstra, HexBoard hexBoard)
{
	if (dijkstra.redWin(hexBoard.getBoard(), hexBoard.getBoardSize()))
	{
		std::cout << "Red wins" << std::endl;
		hexBoard.gameOver();
		return true;
	}
	return false;
}

//Check if blue wins
bool TurnManager::checkBlue(Dijkstra dijkstra, HexBoard hexBoard)
{
	if (dijkstra.blueWin(hexBoard.getBoard(), hexBoard.getBoardSize()))
	{
		std::cout << "Blue wins" << std::endl;
		hexBoard.gameOver();
		return true;
	}

	return false;
}

//Swap players
std::string TurnManager::nextPlayer(std::string player)
{
	if (player == "Player")
	{
		return "AI";
	}
	return "Player";
}