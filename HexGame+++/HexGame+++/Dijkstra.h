/*
Class for pathfinding
*/
#pragma once

#include <vector>

class Dijkstra
{
private:
	struct Coordinate
	{
		int row;
		int col;
	};

	enum neighbours
	{
		topLeft,
		topRight,
		left,
		right,
		botLeft,
		botRight,
		currentCoordinate
	};
	std::vector<Coordinate> checkedCoordinates;

public:
	Dijkstra();
	~Dijkstra();

	bool redWin(std::vector<std::vector<char>> boardState, int boardSize);
	bool blueWin(std::vector<std::vector<char>> boardState, int boardSize);
	bool checkIfWon(char color, std::vector<std::vector<char>> boardState, Coordinate current, int boardSize);
	bool done(Coordinate coordinate);
	char getColor(Coordinate coordinate, const std::vector<std::vector<char>>& boardState);
	Coordinate getNeighbour(Coordinate currentCoordinate, neighbours direction);
};

