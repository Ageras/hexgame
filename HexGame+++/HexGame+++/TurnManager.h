/*
Class for managing turns, call to swap players, check for winners, etc...
*/
#pragma once
#include "HexBoard.h"
#include "InputListener.h"
#include "Minimax.h"
#include "Dijkstra.h"

#include <iostream>
#include <string>

class TurnManager
{
public:

	static void playerTurn(HexBoard& hexBoard, InputListener& inputListener, char& user, std::string& player, int& turn);
	static void aiTurn(HexBoard& hexBoard, InputListener& inputlistener, char& user, int& turn, std::string& player);
	static bool checkRed(Dijkstra dijkstra, HexBoard hexBoard);
	static bool checkBlue(Dijkstra dijkstra, HexBoard hexBoard);
	static std::string nextPlayer(std::string player);

private:
	TurnManager();
	~TurnManager();
};