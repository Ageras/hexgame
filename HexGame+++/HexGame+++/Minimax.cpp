#include "Minimax.h"
#include "MonteCarlo.h"
#include <string>
#include <iostream>


Minimax::Minimax()
{
}

Minimax::~Minimax()
{
}

//Determine the score of a move
int Minimax::score(std::vector<std::vector<char>>& gameState, int row, int col, int& depth, char& color, int boardSize)
{
	//-------------------------------------------------------------------------------------------------------------------------------------
	//CODE FOR ALGHORITHM USED IN TOURNAMENT
	char oppositeColor;
	int result = 0;
	if (color == 'R')
	{
		oppositeColor = 'B';
	}
	else
	{
		oppositeColor = 'R';
	}
	if (color == 'R')
	{
		//Pretend the opponent would place something here next turn, does he win?
		(gameState[row])[col] = oppositeColor;
		// If the current color is red and blue can win, lower score
		if (dijkstra.blueWin(gameState, boardSize))
		{
			result = 5;
		}
		//Set the new turn on the gameState and check if you win
		(gameState[row])[col] = color;
		// If the current color is red and red can win, high score
		if (dijkstra.redWin(gameState, boardSize))
		{
			return 10;
		}
	}
	else
	{
		//Pretend the opponent would place something here next turn, does he win?
		(gameState[row])[col] = oppositeColor;
		// If the current color is blue and red can win, lower score
		if (dijkstra.redWin(gameState, boardSize))
		{
			result = 5;
		}
		//Set the new turn on the gameState and check if you win
		(gameState[row])[col] = color;
		// If the current color is blue and blue can win, high score
		if (dijkstra.blueWin(gameState, boardSize))
		{
			return 10;
		}
	}
	return result;
	//-------------------------------------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------------------------------------
	//CODE FOR CONCURRENT PROGRAMMING ASSIGNMENT
	//MonteCarlo monteCarlo = MonteCarlo();
	//return monteCarlo.calcScore(gameState, boardSize, color);
	//-------------------------------------------------------------------------------------------------------------------------------------
}

Minimax::Move Minimax::minimax(std::vector<std::vector<char>> gameState, int depth, char& color, int boardSize, std::string player)
{
	std::vector<int> scores;
	std::vector<Move> moves;
	//Determine where you can put a new move
	std::vector<std::vector<bool>> possibleMoves = getAvailableMoves(gameState, boardSize);
	int maxScoreIndex = -100;
	int maxScore = -100;

	for (int i = 0; i < boardSize; i++)
	{
		for (int j = 0; j < boardSize; j++)
		{
			//Loop through the board and see if you can put your move here
			if ((possibleMoves[i])[j] == true)
			{
				//Determine the score of the move i,j
				scores.push_back(score(gameState, i, j, depth, color, boardSize));
				move.row = i;
				move.col = j;
				moves.push_back(move);
			}
		}
	}

	for (int max = 0; max < scores.size(); max++)
	{
		if (scores[max] > maxScore)
		{
			maxScoreIndex = max;
			maxScore = scores[max];
		}
	}
	//Return best move
	return moves[maxScoreIndex];
}

std::vector<std::vector<bool>> Minimax::getAvailableMoves(std::vector<std::vector<char>> gameState, int  boardSize)
{
	std::vector<std::vector<bool>> moves(boardSize, std::vector<bool>(boardSize));
	for (int i = 0; i < boardSize; i++)
	{
		for (int j = 0; j < boardSize; j++)
		{
			if ((gameState[i])[j] == '-')
			{
				//Loop through the whole board and check if the value is '-', set on true if this is the case
				(moves[i])[j] = true;
			}
		}
	}
	//Return the board with updated values
	return moves;
}

//Determine if pie rule should be applied
bool Minimax::pieRule()
{
	std::cout << "Pie rule applied!" << std::endl;
	return true;
}
